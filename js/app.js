var App = {}; // Make sure in global namespace

(function () {
    "use strict";

    /**
     * App.Controller
     */
    App.Controller = {};

        /**
         * App.Controller.Search
         */
        App.Controller.Search = {
            last_search_value: '',

            submitNavSearch: function(dom) {
                var search_value = "";

                App.Controller.Search.last_search_value = dom.getElementsByClassName('search-query')[0].value;

                App.Helper.MainPane.update(
                    App.View.Search.searchStart(App.Controller.Search.last_search_value)
                );

                App.View.Loader.toggle();

                App.Api.Search.findAll(App.Controller.Search.last_search_value).then(App.View.Search.submitNavSearch);

                return false;
            }
        };
        /** END App.Controller.Search **/

        /**
         * App.Controller.Artist
         */
        App.Controller.Artist = {
            clickLoad: function(dom) {
                var spid = dom.href,
                    name = dom.innerText;

                App.Helper.MainPane.update(
                    App.View.Artist.artistStart(name)
                );

                App.View.Loader.toggle();

                App.Api.Lookup.findArtist(spid).then(App.View.Artist.clickLoad);

                return false;
            }
        };
        /** END App.Controller.Artist **/

        /**
         * App.Controller.Album
         * 
         * Here we find events related to an album
         */
        App.Controller.Album = {
            // This the the click event for clicking an album uri
            clickLoad: function(dom) {
                var spid = dom.href,
                    name = dom.innerHTML,
                    artist_name = dom.dataset.artist;

                App.Helper.MainPane.update(
                    App.View.Album.albumStart(name, artist_name)
                );

                App.View.Loader.toggle();

                App.Api.Lookup.findAlbum(spid).then(App.View.Album.clickLoad);

                App.View.Loader.toggle();
            }
        };
        /** END App.Controller.Album **/

        /**
         * App.Controller.Track
         * 
         * Here we find events related to particular track
         */
        App.Controller.Track = {
            // This is the play click, from a track in the table
            clickPlay: function(dom) {
                var spid = dom.dataset.track;

                // Send to the player controller to handle loading/pausing/playing
                App.Controller.Player.togglePlay(spid);
            }
        };
        /** END App.Controller.Track **/

        /**
         * App.Controller.Player
         * 
         * Here we find events related to the player
         */
        App.Controller.Player = {
            /** 
             * This will be the song currently loaded into the player
             * 
             * @var {App.Model.Track}
             */
            currentlyPlaying: {},

            /**
             * Store the current interval for the player progress bar
             */
            playerInterval: false,

            /**
             * Play event that is called from the play button
             *
             * @param {DOMElement}
             * @return {App.Controller.Player}
             */
            clickPlay: function(dom) {
                if (this.currentlyPlaying.track === undefined) {
                    alert("Please choose a song to play first.");
                    return false;
                }

                this.togglePlay(this.currentlyPlaying.track.href);

                return this;
            },

            /**
             * The mousedown event for fastforward
             *
             * @param {DOMElement}
             * @return {App.Controller.Player}
             */
            mouseDownFastForward: function(dom) {
                // Clear the current progress bar, and reset to a faster speed
                this.clearPlayerInterval().setPlayerInterval(100);

                return this;
            },

            /**
             * The mouseup event for stopping the fastforward
             *
             * @param {DOMElement}
             * @return {App.Controller.Player}
             */
            mouseUpFastForward: function(dom) {
                this.clearPlayerInterval(); // Stops fast forward

                // If we were in play mode, return to playing
                if (this.currentlyPlaying.track.state === "play") {
                    this.setPlayerInterval();
                }

                return this;
            },

            /**
             * The mousedown event for starting the rewind
             *
             * @param {DOMElement}
             * @return {App.Controller.Player}
             */
            mouseDownRewind: function(dom) {
                // Clear old interval, start new one faster, and move bar backwards
                this.clearPlayerInterval().setPlayerInterval(100, "backward");

                return this;
            },

            /**
             * The mouseup event to stop the rewind
             *
             * @param {DOMElement}
             * @return {App.Controller.Player}
             */
            mouseUpRewind: function(dom) {
                this.clearPlayerInterval(); // Stop rewind

                // Do we return to playing the track?
                if (this.currentlyPlaying.track.state === "play") {
                    this.setPlayerInterval();
                }

                return this;
            },

            /**
             * Handles toggling the playing of a track
             *
             * @param {String} A spotify URI
             * @return {App.Controller.Player}
             */
            togglePlay: function(spid) {
                // Basically if the request track is not loaded/playing, load and start it
                if (this.currentlyPlaying.track === undefined 
                    || this.currentlyPlaying.track.href === undefined 
                    || (spid !== undefined
                        && this.currentlyPlaying.track.href !== spid)
                ) {
                    Jamesify.log("Track is not loaded, load it, then start");
                    App.View.Loader.toggle();

                    this.loadTrack(spid).then(this.loadedTrackCallback.bind(this));
                } else {
                    Jamesify.log("Track is loaded/paused, so play/pause it");
                    this.loadedTrackCallback(true);
                }

                return this;
            },

            /**
             * Handles what to do when a song is done playing
             *
             * @return {App.Controller.Player}
             */
            endPlaying: function() {
                var queue = App.Controller.Playlists.queueModel, // Grab queue
                    i;

                Jamesify.log("Song has ended.");

                // Stop the progress bar
                this.clearPlayerInterval();

                // Reset the model to be aware its not playing anymore
                this.currentlyPlaying.stopPlaying();

                // Toggle the play button back to play
                App.View.Player.togglePlayButton(this.currentlyPlaying);

                // This whole block determines if we have more songs to continue through
                if (queue.tracks.length > 1) {
                    for (i = 0; i < queue.tracks.length; i+=1) {
                        // Skip the last played song
                        if (queue.tracks[i].track.href !== this.currentlyPlaying.track.href) {
                            continue;
                        }

                        // This must be the end of the play queue
                        if (queue.tracks[i + 1] === undefined) {
                            return this;
                        }

                        // Load next track
                        this.togglePlay(queue.tracks[i + 1].track.href);
                    }
                }

                return this;
            },


            /**
             * The callback for once a song has been loaded
             *
             * @param {Boolean}
             * @return {App.Controller.Player}
             */
            loadedTrackCallback: function(isNew) {
                if (isNew !== true) {
                    this.clearPlayerInterval();
                }

                switch (this.currentlyPlaying.track.state.toLowerCase()) {
                    // If we are in play mode, pause it
                    case 'play':
                        this.clearPlayerInterval();
                        this.currentlyPlaying.track.state = 'pause';
                        break;

                    // If we are in pause, put in play
                    case 'pause':
                        this.setPlayerInterval();
                        this.currentlyPlaying.track.state = 'play';
                        break;
                }

                // Toggle the play button
                App.View.Player.togglePlayButton(this.currentlyPlaying);

                return this;
            },

            /**
             * Clears the current interval for updating the play progress
             *
             * @return {App.Controller.Player}
             */
            clearPlayerInterval: function() {
                if (this.playerInterval) {
                    clearInterval(this.playerInterval);
                }

                return this;
            },

            /**
             * Sets up the interval for updating the player
             *
             * @param {Number}
             * @param {String}
             * @return {App.Controller.Player}
             */
            setPlayerInterval: function(timer, direction) {
                timer = timer || 1000;

                this.playerInterval = setInterval(App.View.Player.updatePosition.bind(App.View.Player, direction), timer);

                return this;
            },

            /**
             * A helper function for loading up the tracks data
             * 
             * @see Jamesify.Promise
             * @param {String} Spotify URI
             * @return {App.Controller.Player}
             */
            loadTrack: function(spid) {
                var promise = new Jamesify.Promise();

                App.Api.Lookup.findTrack(spid).then(function(trackResult) {
                    App.View.Loader.toggle();

                    // Get the currentlyPlaying var setup with the new tracks model
                    App.Controller.Player.currentlyPlaying = new App.Model.Track(trackResult.track);

                    // Update the player HTML with the current artist/song info
                    App.View.Player.updateTrackInfo(App.Controller.Player.currentlyPlaying);

                    promise.resolve();
                });

                return promise;
            }
        };

        /**
         * App.Controller.Playlists
         *
         * Handles events related to the playlists
         */
        App.Controller.Playlists = {
            playlistsModel: false,
            queueModel: false,

            /**
             * The handles the initial page load, to restore items, 
             *   and provide a default Queue object.
             *
             * @return {App.Controller.Playlists}
             */
            loadUp: function() {
                var that = this,
                    queue = new App.Model.Queue(),
                    i;

                App.View.Loader.toggle();

                App.Model.Playlist.restore(queue, queue.href).then(function(queueModel) {
                    that.queueModel = queueModel;

                    that.queueModel.init();

                    App.Model.Playlists.restore().then(function(playlists) {
                        that.playlistsModel = playlists;

                        for (i in that.playlistsModel.playlists) {
                            if (!that.playlistsModel.playlists.hasOwnProperty(i)) {
                                continue;
                            }

                            that.playlistsModel.playlists[i].init();
                        }

                        App.View.Loader.toggle();
                    });
                });

                return this;
            },

            clickCreate: function(dom) {
                var name      = prompt("Please provide a name for your playlist."),
                    playlist  = this.playlistsModel.createNew(name),
                    trackHref = dom.href,
                    that      = this;

                App.Model.Tracks.loadTrack(trackHref).then(function(newTrack) {
                    playlist.addTrack(newTrack); // Add the song to the playlist
                });

                return this;
            },

            clickAddToPlaylist: function(dom) {
                var playlistId = dom.href,
                    trackId    = dom.dataset.track;

                Jamesify.log("Add track " + trackId + " to playlist " + playlistId);
                
                this.getPlaylist(playlistId).addTrackByHref(trackId);

                return this;
            },

            getPlaylist: function(href) {
                if (href === this.queueModel.href) {
                    return this.queueModel;
                } else {
                    return this.playlistsModel.playlists[href];
                }
            },

            clickPlaylist: function(dom) {
                var playlist = this.getPlaylist(dom.href);

                App.View.Loader.toggle();

                playlist.view.draw();

                App.View.Loader.toggle();

                return this;
            },

            clickQueue: function(dom) {
                var queue = this.queueModel,
                    view;

                App.View.Loader.toggle();

                view = App.View.Playlist.showPlaylist(playlist);

                document.getElementById('playlistListing').innerHTML += view.render();

                App.View.Loader.toggle();
            },

            clickRemoveTrack: function(dom) {
                var playlistId = dom.href,
                    trackId    = dom.dataset.track,
                    tableRow   = document.getElementById('row-' + trackId);

                Jamesify.log("Removing track " + trackId + " from playlist " + playlistId);

                this.getPlaylist(playlistId).removeTrack(trackId);

                tableRow.parentNode.removeChild(tableRow);
            },

            clickRename: function(dom) {
                var playlistId = dom.href,
                    newName    = prompt("What should this playlist be called?");

                this.getPlaylist(playlistId).set('name', newName).save();
            },

            clickDelete: function(dom) {
                var playlistId = dom.href,
                    confirmed  = confirm("Are you sure?");

                this.playlistsModel.removePlaylist(playlistId);
                this.clickPlaylist({href: this.queueModel.href});
            },

            addPlaylistLink: function(playlist) {
                var html = App.View.Playlist.createLink(playlist).render();

                document.getElementById('playlists').innerHTML += html;

                return this;
            }
        };
    /** END App.Controller **/

    /**
     * App.Helper
     */
    App.Helper = {};

        /**
         * App.Helper.Search
         */
        App.Helper.Search = {
            displayAlbums: function(searchResults) {
                var view = App.View.Search.albumListing(searchResults.albums, searchResults.info.num_results);
                
                document.getElementById('albumResults').innerHTML = view;
            },

            displayArtists: function(searchResults) {
                var view = App.View.Search.artistListing(searchResults.artists, searchResults.info.num_results);
                
                document.getElementById('artistResults').innerHTML = view;
            },

            displayTracks: function(searchResults) {
                var view = App.View.Search.trackListing(searchResults.tracks, searchResults.info.num_results);
                
                document.getElementById('trackResults').innerHTML = view;
            }
        };
        /** END App.Helper.Search **/

        /**
         * App.Helper.MainPane
         */
        App.Helper.MainPane = {
            update: function(view) {
                var html = view.render();

                App.Helper.MainPane.get().innerHTML = html;
            },

            get: function() {
                return document.getElementById('mainPane');
            },

            showNoResults: function() {
                var html = '<div class="alert alert-error">'
                         + '  Ooops, nothing could be found.'
                         + '</div>'
                ;

                App.Helper.MainPane.get().getElementsByClassName('row-fluid')[0].innerHTML = html;
            }
        };
        /** END App.Helper.MainPane **/

        App.Helper.artistLink = function(href, name) {
            return '<a href="' + href + '" class="artist_name">' + name + '</a>';
        };

    /** END App.Helper **/

    /**
     * App.View
     */
    App.View = {};

        /**
         * App.View.Search
         */
        App.View.Search = {

            submitNavSearch: function (dataArtists, dataAlbums, dataTracks) {
                App.Helper.Search.displayArtists(dataArtists[0]);
                App.Helper.Search.displayAlbums(dataAlbums[0]);
                App.Helper.Search.displayTracks(dataTracks[0]);

                App.View.Loader.toggle();
            },

            searchStart: function(last_search) {
                var html;

                html = '<header class="jumbotron subhead">'
                     + '  <h1>Results</h1>'
                     + '  <p class="lead">Showing results for "{{last_search}}"</p>'
                     + '</header>'
                     + '<div class="row-fluid">'
                     + '  <div id="albumResults" class="span4"></div>'
                     + '  <div id="artistResults" class="span4"></div>'
                     + '  <div id="trackResults" class="span4"></div>'
                     + '</div>';

                return new Jamesify.View(html, {last_search: last_search});
            },

            albumListing: function(albums, total_items) {
                var html = '',
                    view = new Jamesify.View(),
                    items = Array.prototype.slice.call(albums),
                    children = [],
                    i;

                html = "\n\t"
                     + '<h3>Albums <span class="badge">{{total_items}}</span></h3>'
                     + '<ul>{{children}}</ul>'
                ;

                for (i = 0; i < items.length; i+=1) {
                    children.push(App.View.Search.albumItem(items[i]));
                }

                return view.assign({total_items: total_items, children: children}).render(html);
            },

            albumItem: function(album) {
                var html = '<li>'
                         + '<a href="{{spid}}" class="album_name" data-artist="{{artist_name}}">{{album_name}}</a>'
                         + '</li>',
                    artists = [],
                    i
                ;

                for (i = 0; i < album.artists.length; i+=1) {
                    artists.push(album.artists[i].name);
                }

                return new Jamesify.View(html, {
                    album_name : album.name, 
                    spid       : album.href,
                    artist_name: artists.join(", ")
                });
            },

            artistListing: function(artists, total_items) {
                var html = '',
                    view = new Jamesify.View(),
                    items = Array.prototype.slice.call(artists),
                    children = [],
                    i
                ;

                html = '<h3>Artists <span class="badge">{{total_items}}</span></h3>'
                     + '<ul>{{children}}</ul>'
                ;

                for (i = 0; i < items.length; i+=1) {
                    children.push(App.View.Search.artistItem(items[i]));
                }

                return view.assign({total_items: total_items, children: children}).render(html);
            },

            artistItem: function(artist) {
                var html = '<li>'
                         + '<a href="{{spid}}" class="artist_name">{{name}}</a>'
                         + '</li>'
                ;

                return new Jamesify.View(html, {name: artist.name, spid: artist.href});
            },

            trackListing: function(tracks, total_items) {
                var html = '',
                    view = {},
                    items = Array.prototype.slice.call(tracks),
                    children = [],
                    i
                ;

                html = '<h3>Tracks <span class="badge">{{total_items}}</span></h3>'
                     + '<ul>{{children}}</ul>'
                ;

                for (i = 0; i < items.length; i+=1) {
                    children.push(App.View.Search.trackItem(items[i]));
                }

                view = new Jamesify.View(html, {total_items: total_items, children: children});

                return view.render();
            },

            trackItem: function(track) {
                var html = '<li>'
                         + '<a href="{{spid}}" class="track_name">{{track_name}}</a>'
                         + '</li>'
                ;

                return new Jamesify.View(html, {track_name: track.name, spid: track.href});
            }
        };
        /** END App.View.Search **/

        /**
         * App.View.Artist
         */
        App.View.Artist = {

            artistStart: function(artist_name) {
                var html = '<header class="jumbotron subhead">'
                         + '  <h1>{{artist_name}}</h1>'
                         + '  <p class="lead">Albums & Singles</p>'
                         + '</header>'
                         + '<div class="row-fluid">'
                         + '  <div id="artistListing"></div>'
                         + '</div>'
                ;

                return new Jamesify.View(html, {artist_name: artist_name});
            },

            clickLoad: function(artist) {
                App.View.Loader.toggle();

                if (!artist.artist.albums.length) {
                    App.Helper.MainPane.showNoResults();
                }

                App.View.Album.
                         setDom(document.getElementById('artistListing')).
                         displayAlbums(artist.artist.albums);
            },
        };
        /** END App.View.Artist **/

        /**
         * App.View.Album
         */
        App.View.Album = {
            dom: {},

            setDom: function(dom) {
                this.dom = dom;

                return this;
            },

            displayAlbums: function(albums) {
                var ul = document.createElement('ul'),
                    html = '',
                    i
                ;
                
                for (i = 0; i < albums.length; i+=1) {
                    html += this.displayAbumsItem(albums[i].album);
                }

                ul.innerHTML = html;

                this.dom.appendChild(ul);
            },

            displayAbumsItem: function(album) {
                var html = '<li>' 
                         + '  <a href="{{al_href}}" class="album_name" data-artist="{{ar_name}}">{{al_name}}</a>'
                         + '  <small>by <a href="{{ar_href}}" class="artist_name">{{ar_name}}</a></small>'
                         + '</li>',
                    view = new Jamesify.View(html, {
                        ar_name: album.artist, 
                        ar_href: album["artist-id"],
                        al_name: album.name,
                        al_href: album.href
                    });

                return view.render();
            },

            albumStart: function(name, artist_name) {
                var html = '<header class="jumbotron subhead">'
                         + '  <h1>{{al_name}}</h1>'
                         + '  <p class="lead">{{ar_name}}</p>'
                         + '</header>'
                         + '<div class="row-fluid">'
                         + '  <div id="albumListing"></div>'
                         + '</div>';

                return new Jamesify.View(html, {al_name: name, ar_name: artist_name});
            },

            clickLoad: function(albumResult) {
                var html = '<p>Released: {{release_date}}</p>'
                         + '<table class="table">'
                         + '  <thead>'
                         + '    <tr>'
                         + '      <th>Disc</th>'
                         + '      <th>Track</th>'
                         + '      <th></th>'
                         + '      <th>Title</th>'
                         + '      <th>Artists</th>'
                         + '      <th>Popularity</th>'
                         + '      <th>Length</th>'
                         + '    </tr>'
                         + '  </thead>'
                         + '  <tbody id="trackListing">'
                         + '    {{children}}'
                         + '  </tbody>'
                         + '</table>',
                    view = new Jamesify.View(html),
                    tracks = [],
                    playlistViews = App.View.Playlists.dropdown(),
                    i,
                    track;

                // App.View.Loader.toggle();

                for (i = 0; i < albumResult.album.tracks.length; i+=1) {
                    track = new App.Model.Track(albumResult.album.tracks[i]);

                    tracks.push(App.View.Album.trackItem(track, playlistViews));
                }
                
                document.getElementById('albumListing').innerHTML = view.assign({
                    children: tracks, 
                    release_date: albumResult.album.released
                }).render();
            },

            trackItem: function(trackModel, playlistsHtml) {
                var html = '<tr>'
                         + '  <td>{{disc}}</td>'
                         + '  <td>{{track}}</td>'
                         + '  <td>'
                         + '    <div class="btn-group">'
                         + '      <button class="btn play" data-track="{{track_href}}">Play</button>'
                         + '      <button class="btn dropdown-toggle" data-toggle="dropdown">'
                         + '        <span class="caret"></span>'
                         + '      </button>'
                         + '      <ul class="dropdown-menu">'
                         + '        <li><a href="{{track_href}}" class="create_playlist">Create playlist</a></li>'
                         + '        <li class="divider"></li>'
                         + '        ' + playlistsHtml
                         + '      </ul>'
                         + '    </div>'
                         + '  </td>'
                         + '  <td>{{title}}</td>'
                         + '  <td>{{artists}}</td>'
                         + '  <td>'
                         + '    <div class="progress progress-success">'
                         + '      <div class="bar" style="width: {{pop}}%"></div>'
                         + '    </div>'
                         + '  </td>'
                         + '  <td>{{length}}</td>'
                         + '</tr>',
                    i;

                return new Jamesify.View(html, {
                    disc       : trackModel.track["disc-number"],
                    track      : trackModel.track["track-number"],
                    title      : trackModel.track.name,
                    artists    : trackModel.getArtistsString(),
                    pop        : trackModel.getPopularityPercentage(),
                    length     : trackModel.getCleanDuration(),
                    track_href : trackModel.track.href
                });
            }
        };
        /** END App.View.Album **/

        /**
         * App.View.Player
         */
        App.View.Player = {
            playDirection: "forward",

            updatePosition: function(direction) {
                var currentTrack = App.Controller.Player.currentlyPlaying;

                direction = direction || "forward";

                this.playDirection = direction;

                if ((currentTrack.track.currentPosition - 1) < currentTrack.track.length) {
                    this.updateBar(currentTrack).updateTimer(currentTrack);
                } else {
                    return App.Controller.Player.endPlaying();
                }

                // Update current position
                if (this.playDirection === "forward") {
                    currentTrack.track.currentPosition+=1;
                } else {
                    currentTrack.track.currentPosition-=1;
                }
            },

            updateBar: function(currentTrack) {
                var el = document.getElementById('playerProgressBar');

                el.style.width = currentTrack.getPercentComplete() + "%";

                return this;
            },

            updateTimer: function(currentTrack) {
                var el = document.getElementById('playerTimer');

                el.innerHTML = currentTrack.getCleanPosition() + " / " + currentTrack.getCleanDuration();

                return this;
            },

            updateTrackInfo: function(currentTrack) {
                var el = document.getElementById('playingInfo');

                el.innerHTML = currentTrack.getArtistsString() + " - " + currentTrack.track.name;

                return this;
            },

            togglePlayButton: function(currentTrack) {
                var el = document.getElementById('playerPlayButton'),
                    otherState = (currentTrack.track.state.toLowerCase() === "play") ? "pause" : "play",
                    regexp = new RegExp("icon-" + currentTrack.track.state.toLowerCase());

                el.firstElementChild.className = el.firstElementChild.className.replace(regexp, "icon-" + otherState);

                return this;
            }
        };
        /** END App.View.Player **/

        App.View.Loader = {
            isShowing: false,

            toggle: function() {
                var el = document.getElementById('loading');

                if (el.style.display === '' || el.style.display === 'none') {
                    el.style.display = 'block';
                } else if (el.style.display === 'block') {
                    el.style.display = 'none';
                }

                this.isShowing = (el.style.display === 'none') ? false : true;
            }
        };

        App.View.SmartPlaylist = function(playlistModel) {
            this.model = playlistModel;

            this.allParams = {};

            this.mainHtml = '<header class="jumbotron subhead">'
                          + '  <h1>{{pl_name}}</h1>'
                          + '    {{editor}}'
                          + '    {{deletor}}'
                          + '</header>'
                          + '<div class="row-fluid">'
                          + '  <div id="playlistListing">'
                          + '    <table class="table">'
                          + '      <thead>'
                          + '        <tr>'
                          + '          <th>Track</th>'
                          + '          <th></th>'
                          + '          <th>Title</th>'
                          + '          <th>Artists</th>'
                          + '          <th>Album</th>'
                          + '          <th>Popularity</th>'
                          + '          <th>Length</th>'
                          + '          <th></th>'
                          + '        </tr>'
                          + '      </thead>'
                          + '    <tbody id="trackListing">'
                          + '      {{children}}'
                          + '    </tbody>'
                          + '  </table>'
                          + '</div>';
        };

            App.View.SmartPlaylist.prototype.draw = function() {
                var subview = new Jamesify.View();

                this.build();

                App.Helper.MainPane.update(new Jamesify.View(this.mainHtml, this.allParams));

                return this;
            };

            App.View.SmartPlaylist.prototype.build = function() {
                this.allParams.pl_name = this.model.name;
                this.allParams.pl_href = this.model.href;
                this.allParams.children = this.buildTracks();
                this.allParams.editor = "";
                this.allParams.deletor = ""

                if (this.model.isDeletable) {
                    this.allParams.editor = (new Jamesify.View('<a href="{{pl_href}}" class="btn btn-mini playlist_rename">Rename</a>', this.allParams)).render();
                }

                if (this.model.isEditable) {
                    this.allParams.deletor = (new Jamesify.View('<a href="{{pl_href}}" class="btn btn-mini btn-danger playlist_delete">Delete</a>', this.allParams)).render();
                }
            };

            App.View.SmartPlaylist.prototype.buildTracks = function() {
                var tracks = [],
                    i;

                for (i = 0; i < this.model.tracks.length; i+=1) {
                    tracks.push(this.buildTrack(i));
                }

                return tracks.join("\n");
            };

            App.View.SmartPlaylist.prototype.buildTrack = function(trackKey) {
                var html = '<tr id="row-{{track_href}}">'
                         + '  <td>{{track}}</td>'
                         + '  <td>'
                         + '    <div class="btn-group">'
                         + '      <button class="btn play" data-track="{{track_href}}">Play</button>'
                         + '      <button class="btn dropdown-toggle" data-toggle="dropdown">'
                         + '        <span class="caret"></span>'
                         + '      </button>'
                         + '      <ul class="dropdown-menu">'
                         + '        <li><a href="{{track_href}}" class="create_playlist">Create playlist</a></li>'
                         + '        <li class="divider"></li>'
                         + '        ' + App.View.Playlists.dropdown()
                         + '      </ul>'
                         + '    </div>'
                         + '  </td>'
                         + '  <td>{{title}}</td>'
                         + '  <td>{{artists}}</td>'
                         + '  <td>{{album}}</td>'
                         + '  <td>'
                         + '    <div class="progress progress-success">'
                         + '      <div class="bar" style="width: {{pop}}%"></div>'
                         + '    </div>'
                         + '  </td>'
                         + '  <td>{{length}}</td>'
                         + '  <td><a href="{{pl_href}}" class="btn btn-mini btn-danger remove_track" data-track="{{track_href}}"><i class="icon-remove icon-white"></i></a></td>'
                         + '</tr>',
                    artistViews = [], 
                    artist,
                    i;

                for (i = 0; i < this.model.tracks[trackKey].track.artists.length; i+=1) {
                    artist = this.model.tracks[trackKey].track.artists[i];
                    artistViews.push(App.Helper.artistLink(artist.href, artist.name));
                }

                return new Jamesify.View(html, {
                    track      : trackKey + 1,
                    title      : this.model.tracks[trackKey].track.name,
                    artists    : artistViews.join(", "),
                    pop        : this.model.tracks[trackKey].getPopularityPercentage() + "%",
                    length     : this.model.tracks[trackKey].getCleanDuration(),
                    track_href : this.model.tracks[trackKey].track.href,
                    pl_href    : this.model.href,
                    album      : '<a href="' + this.model.tracks[trackKey].track.album.href + '" class="album_name" data-artist="' + "---" + '">' + this.model.tracks[trackKey].track.album.name + '</a>'
                }).render();
            };

        App.View.PlaylistNav = function(playlistModel) {
            this.model = playlistModel;

            this.allParams = {};

            this.html = '<li id="li-{{href}}">'
                      + '  <a href="{{href}}" class="playlist_click">'
                      + '    <i class="icon-th-list"></i>'
                      + '    <span id="{{href}}-name">{{name}}</span>'
                      + '  </a>'
                      + '</li>';
        };

            App.View.PlaylistNav.prototype.draw = function() {
                var view = new Jamesify.View(this.html, {
                    name: this.model.name,
                    href: this.model.href
                });

                document.getElementById('playlists').innerHTML += view.render();

                return this;
            };

            App.View.PlaylistNav.prototype.remove = function() {
                var li = document.getElementById('li-' + this.model.href);

                li.parentNode.removeChild(li);
            };

            App.View.PlaylistNav.prototype.updateName = function() {
                var dom = document.getElementById(this.model.href + "-name");

                if (dom !== undefined) {
                    dom.innerText = this.model.name;
                }

                return this;
            };

        App.View.Playlists = {
            dropdown: function() {
                var playlists = App.Controller.Playlists.playlistsModel.playlists,
                    playlistViews = [],
                    i;

                playlistViews.push('<li><a href="' 
                                    + App.Controller.Playlists.queueModel.href 
                                    + '" class="add_to_playlist" data-track="{{track_href}}">' 
                                    + App.Controller.Playlists.queueModel.name + '</a></li>');

                for (i in playlists) {
                    if (!playlists.hasOwnProperty(i)) {
                        continue;
                    }

                    playlistViews.push('<li><a href="' + playlists[i].href + '" class="add_to_playlist" data-track="{{track_href}}">' + playlists[i].name + '</a></li>');
                }

                return playlistViews.join("\n");
            }
        };
    /** END App.View **/

    /**
     * App.Api
     */
    App.Api = {};

        /**
         * App.Api.Search
         */
        App.Api.Search = function (type, searchString) {
            this.search_type   = type;
            this.searchString  = searchString;
            this.full_endpoint = App.Api.Search.api_endpoint + type + '.json?q=' + encodeURIComponent(searchString);
        };

            App.Api.Search.prototype = new Jamesify.Api();

            App.Api.Search.api_endpoint = 'http://ws.spotify.com/search/1/';

            App.Api.Search.findAll = function (name) {
                var findArtist = App.Api.Search.findArtist(name),
                    findAlbum  = App.Api.Search.findAlbum(name),
                    findTrack  = App.Api.Search.findTrack(name);

                return Jamesify.Promise.when(findArtist, findAlbum, findTrack);
            };

            App.Api.Search.findArtist = function (name) {
                var search = new App.Api.Search('artist', name);

                return search.execute();
            };

            App.Api.Search.findAlbum = function (name) {
                var search = new App.Api.Search('album', name);

                return search.execute();
            };

            App.Api.Search.findTrack = function (name) {
                var search = new App.Api.Search('track', name);

                return search.execute();
            };
        /** END App.Api.Search **/

        /**
         * App.Api.Lookup
         */
        App.Api.Lookup = function (spid, extras) {
            this.spid = spid;
            this.full_endpoint = App.Api.Lookup.api_endpoint + encodeURIComponent(spid);

            if (typeof extras === "string") {
                this.full_endpoint += "&extras=" + extras;
            }
        };

            App.Api.Lookup.prototype = new Jamesify.Api();

            App.Api.Lookup.api_endpoint = 'http://ws.spotify.com/lookup/1/.json?uri=';

            App.Api.Lookup.findArtist = function (spid) {
                var lookup = new App.Api.Lookup(spid, "albumdetail");

                return lookup.execute();
            };

            App.Api.Lookup.findAlbum = function (spid) {
                var lookup = new App.Api.Lookup(spid, "trackdetail");

                return lookup.execute();
            };

            App.Api.Lookup.findTrack = function (spid) {
                var lookup = new App.Api.Lookup(spid);

                return lookup.execute();
            };
        /** END App.Api.Lookup **/

    /** END App.Api **/

    /**
     * App.Model
     */
    App.Model = {};

        App.Model.Tracks = {
            tracks: {},

            addTrack: function(trackModel) {
                var spid = trackModel.track.href;

                if (this.tracks[spid] === undefined) {
                    this.tracks[spid] = trackModel;
                }

                return this;
            },

            loadTrack: function(spid) {
                var promise = new Jamesify.Promise();

                if (!(this.tracks[spid] instanceof App.Model.Track)) {
                    App.Api.Lookup.findTrack(spid).then(function(data) {
                        this.tracks[spid] = new App.Model.Track(data.track);

                        promise.resolve(this.tracks[spid]);
                    }.bind(this));
                } else {
                    promise.resolve(this.tracks[spid]);
                }

                return promise;
            }
        };

        /**
         * App.Model.Track
         */
        App.Model.Track = function(track) {
            this.track = track;
            this.track.currentPosition = 0;
            this.track.state = 'pause';
        };

            App.Model.Track.prototype = new Jamesify.Model();

            App.Model.Track.prototype.stopPlaying = function() {
                this.track.state = 'pause';
                this.track.currentPosition = 0;
            };

            App.Model.Track.prototype.getCleanDuration = function() {
                if (this.track.cleanDuration === undefined) {
                    var minTotal = Math.floor(this.track.length / 60),
                        secTotal = Math.ceil(this.track.length - (minTotal * 60));

                    this.track.cleanDuration = minTotal + ":" + ((secTotal >= 10) ? "" : "0") + secTotal;
                }

                return this.track.cleanDuration;
            };

            App.Model.Track.prototype.getCleanPosition = function() {
                var minPosition = Math.floor(this.track.currentPosition / 60),
                    secPosition = Math.ceil(this.track.currentPosition - (minPosition * 60));

                this.track.cleanPosition = minPosition + ":" + ((secPosition >= 10) ? "" : "0") + secPosition;

                return this.track.cleanPosition;
            };

            App.Model.Track.prototype.getPercentComplete = function() {
                return (this.track.currentPosition / this.track.length * 100).toFixed(2);
            };

            App.Model.Track.prototype.getPopularityPercentage = function() {
                if (this.track.popularityPercentage === undefined) {
                    this.track.popularityPercentage = (this.track.popularity * 100).toFixed(2);
                }

                return this.track.popularityPercentage;
            };

            App.Model.Track.prototype.getArtistsString = function() {
                var artist,
                    artistViews = [],
                    i;

                for (i = 0; i < this.track.artists.length; i+=1) {
                    artist = this.track.artists[i];
                    artistViews.push(App.Helper.artistLink(artist.href, artist.name));
                }

                return artistViews.join("\n");
            };

            App.Model.Track.restore = function(trackHref) {
                var trackJson = Jamesify.Model.prototype.cacheGet(trackHref),
                    trackInstance = {},
                    promise = new Jamesify.Promise();

                if (trackJson === null) {
                    App.Api.Lookup.findTrack(trackHref).then(function(data) {
                        trackInstance = new App.Model.Track(data.track);

                        App.Model.Tracks.addTrack(trackInstance);

                        promise.resolve(trackInstance);
                    });
                } else {
                    trackInstance = Jamesify.Model.restore(new App.Model.Track({}), trackJson);

                    App.Model.Tracks.addTrack(trackInstance);

                    promise.resolve(trackInstance);
                }

                return promise;
            };
        /** END App.Model.Track **/

        /**
         * App.Model.Playlists
         */
        App.Model.Playlists = function() {
            this.playlists = {};
            this.href      = App.Model.Playlists.href;
        };

            App.Model.Playlists.href = "app.model.playlists";

            App.Model.Playlists.prototype = new Jamesify.Model();

            App.Model.Playlists.prototype.createNew = function(name) {
                var newPlaylist = new App.Model.Playlist(name);

                this.playlists[newPlaylist.href] = newPlaylist;

                newPlaylist.save(); // Save new playlist

                this.save(); // Save new playlists

                newPlaylist.init();

                return newPlaylist;
            };

            App.Model.Playlists.prototype.removePlaylist = function(href) {
                this.cacheDelete(href);

                this.playlists[href].viewNav.remove();

                delete this.playlists[href];

                this.save();

                return this;
            };

            App.Model.Playlists.prototype.save = function() {
                var data = {}, i;

                data.href = this.href;
                data.playlists = [];

                for (i in this.playlists) {
                    if (!this.playlists.hasOwnProperty(i)) {
                        continue;
                    }

                    data.playlists.unshift(this.playlists[i].href);
                }

                this.cacheSave(this.href, data);

                return this;
            };

            App.Model.Playlists.restore = function() {
                var promise = new Jamesify.Promise(),
                    href = this.href;

                // Do a timeout here, with 0, to break this into the
                // event loop, but make it happen at the next tick
                setTimeout(function() {
                    var playlistJson = Jamesify.Model.prototype.cacheGet(href) || {},
                        instance     = new App.Model.Playlists(),
                        data = {playlists: {}},
                        restorePromises = [], // I made a funny, haha
                        playlistsModel,
                        i;

                    if (playlistJson.hasOwnProperty("playlists") && playlistJson.playlists.length) {
                        for (i = 0; i < playlistJson.playlists.length; i+=1) {
                            restorePromises.unshift(App.Model.Playlist.restore(new App.Model.Playlist(), playlistJson.playlists[i]));
                        }
                    }

                    if (restorePromises.length > 0) {
                        Jamesify.Promise.when.apply(null, restorePromises).then(function() {
                            for (i = 0; i < arguments.length; i+=1) {
                                data.playlists[arguments[i][0].href] = arguments[i][0];
                            }

                            playlistsModel = Jamesify.Model.restore(instance, data);

                            promise.resolve(playlistsModel);
                        });
                    } else {
                        promise.resolve(instance);
                    }
                }, 0);

                return promise;
            };
        /** END App.Model.Playlists **/

        /**
         * App.Model.Playlist
         *
         * Stores data about the playlist, importantly the tracks that are in it
         */
        App.Model.Playlist = function(name) {
            Jamesify.Model.call(this);

            this.href   = false;
            this.name   = false;
            this.tracks = [];
            this.setName(name);
            this.createHref();
            this.view = new App.View.SmartPlaylist(this);
            this.viewNav = new App.View.PlaylistNav(this);
            this.isDeletable = true;
            this.isEditable = true;
        };

            App.Model.Playlist.prototype = new Jamesify.Model();

            App.Model.Playlist.prototype.init = function() {
                this.viewNav.draw(); // Initialize

                // Attach observers to our playlist views so they will be changed anytime the name field changes
                this.addObserver('name', this.view.draw.bind(this.view));
                this.addObserver('name', this.viewNav.updateName.bind(this.viewNav));
            };

            App.Model.Playlist.prototype.createHref = function() {
                if (this.href === false) {
                    this.href = "app.model.playlist:" + (new Date()).toJSON();
                }

                return this;
            };

            App.Model.Playlist.prototype.setName = function(name) {
                var date;

                name = name || false;

                if (name === false) {
                    date = new Date();

                    name = "Your life @ " + date.getMonth() 
                         + "/" + date.getDay() 
                         + "/" + date.getFullYear()
                         + " " + date.getHours()
                         + ":" + date.getMinutes()
                         + ":" + date.getSeconds()
                         + ":" + date.getMilliseconds();
                }

                this.name = name;

                return this;
            };

            App.Model.Playlist.prototype.addTrackByHref = function(href) {
                App.Model.Tracks.loadTrack(href).then(function(trackModel) {
                    this.addTrack(trackModel);
                }.bind(this));

                return this;
            };

            App.Model.Playlist.prototype.addTrack = function(track) {
                if (!(track instanceof App.Model.Track)) {
                    throw 'App.Model.Playlist.addTrack : "track" is not a valid instance of App.Model.Track.';
                }

                this.tracks.push(track);

                this.save();

                return this;
            };

            App.Model.Playlist.prototype.removeTrack = function(href) {
                var i;
                
                for (i = 0; i < this.tracks.length; i+=1) {
                    if (this.tracks[i].track.href === href) {
                        this.tracks.splice(i, 1);
                        this.save();
                        break;
                    }
                }

                return this;
            };

            /**
             * Save the playlist to the cache storage.
             *
             * Making sure to only save certain pieces of data
             */
            App.Model.Playlist.prototype.save = function() {
                var data = {}, 
                    i;

                data.href = this.href;
                data.name = this.name;
                data.tracks = [];

                for (i = 0; i < this.tracks.length; i+=1) {
                    data.tracks.push(this.tracks[i].track.href);
                }

                // Save to storage
                this.cacheSave(this.href, data);
            };

            /**
             * This will restore a playlist object from the provided playlistHref/id
             *    And by restore, I mean load it back from storage
             */
            App.Model.Playlist.restore = function(playlistInstance, playlistHref) {
                var playlist = Jamesify.Model.prototype.cacheGet(playlistHref) || {},
                    restorePromises = [], // haha
                    promise = new Jamesify.Promise(),
                    i;

                if (playlist.hasOwnProperty("tracks") && playlist.tracks.length) {
                    for (i = 0; i < playlist.tracks.length; i+=1) {
                        restorePromises.push(App.Model.Track.restore(playlist.tracks[i]));
                    }
                }

                // Reset tracks array
                delete playlist.tracks;
                playlist.tracks = [];

                if (restorePromises.length) { // Looks like we had some tracks to restore
                    Jamesify.Promise.when.apply(null, restorePromises).then(function() {
                        for (i = 0; i < arguments.length; i+=1) {
                            playlist.tracks.push(arguments[i][0]); // For now i know the first arg is the instance
                        }

                        promise.resolve(Jamesify.Model.restore(playlistInstance, playlist));
                    });
                } else { // No tracks yet
                    promise.resolve(Jamesify.Model.restore(playlistInstance, playlist));
                }

                return promise;
            };
        /** END App.Model.Playlist **/

        /**
         * App.Model.Queue
         *
         * Inherits from App.Mode.Playlist
         *
         * This is a special playlist, that is not allowed to be renamed or deleted
         */
        App.Model.Queue = function() {
            App.Model.Playlist.call(this, 'Play Queue');

            this.href = 'app.model.queue:play_queue';
            this.view = new App.View.SmartPlaylist(this);
            this.viewNav = new App.View.PlaylistNav(this);

            this.isDeletable = false;
            this.isEditable = false;
        };

            App.Model.Queue.prototype = new App.Model.Playlist();

            /**
             * This has been overloaded to skip the adding of observers
             */ 
            App.Model.Queue.prototype.init = function() {
                this.viewNav.draw(); // Initialize
            };

    /** END App.Model **/
}());