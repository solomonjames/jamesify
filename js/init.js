
(function() {
    "use strict";

    var submitRoutes = new Jamesify.Router(),
        clickRoutes  = new Jamesify.Router(),
        mouseDownRoutes = new Jamesify.Router(),
        mouseUpRoutes   = new Jamesify.Router();
    
    // Submit event based routes
    submitRoutes.addRoute('top_left_search', {classes: ["navbar-search"]}, App.Controller.Search.submitNavSearch);

    // Click event based routes
    clickRoutes
        .addRoute('artist_name', {classes: ["artist_name"]}, App.Controller.Artist.clickLoad.bind(App.Controller.Artist))
        .addRoute('album_name',  {classes: ["album_name"]},  App.Controller.Album.clickLoad.bind(App.Controller.Album))
        .addRoute('play_track',  {classes: ["play"]},    App.Controller.Track.clickPlay.bind(App.Controller.Track))
        .addRoute('player_play', {id: "playerPlayButton", allowChildren: true}, App.Controller.Player.clickPlay.bind(App.Controller.Player))
        .addRoute('create_playlist',  {classes: ["create_playlist"]}, App.Controller.Playlists.clickCreate.bind(App.Controller.Playlists))
        .addRoute('playlist_click', {classes: ["playlist_click"], allowChildren: true}, App.Controller.Playlists.clickPlaylist.bind(App.Controller.Playlists))
        .addRoute('add_to_playlist', {classes: ["add_to_playlist"]}, App.Controller.Playlists.clickAddToPlaylist.bind(App.Controller.Playlists))
        .addRoute('remove_track', {classes: ["remove_track"], allowChildren: true}, App.Controller.Playlists.clickRemoveTrack.bind(App.Controller.Playlists))
        .addRoute('playlist_rename', {classes: ["playlist_rename"], allowChildren: true}, App.Controller.Playlists.clickRename.bind(App.Controller.Playlists))
        .addRoute('playlist_delete', {classes: ["playlist_delete"], allowChildren: true}, App.Controller.Playlists.clickDelete.bind(App.Controller.Playlists));

    // Mouse Down events
    mouseDownRoutes
        .addRoute('fastforward', {classes: ["btn-fastforward"], allowChildren: true}, App.Controller.Player.mouseDownFastForward.bind(App.Controller.Player))
        .addRoute('rewind', {classes: ["btn-rewind"], allowChildren: true}, App.Controller.Player.mouseDownRewind.bind(App.Controller.Player));

    // Mouse Up events
    mouseUpRoutes
        .addRoute('fastforward', {classes: ["btn-fastforward"], allowChildren: true}, App.Controller.Player.mouseUpFastForward.bind(App.Controller.Player))
        .addRoute('rewind', {classes: ["btn-rewind"], allowChildren: true}, App.Controller.Player.mouseUpRewind.bind(App.Controller.Player));

    Jamesify.Router
            .addRouter('click', clickRoutes)
            .addRouter('submit', submitRoutes)
            .addRouter('mousedown', mouseDownRoutes)
            .addRouter('mouseup', mouseUpRoutes);

    App.Controller.Playlists.loadUp();
}());