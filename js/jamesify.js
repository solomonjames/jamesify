var Jamesify = {}; // Make sure in global namespace

(function () {
    "use strict"; // Sure, yell at me more, I can handle it

    // Used to enable and disable console.log
    Jamesify.debug = true;

    // Wrapper for the console.log, so we can easily disable them in production
    Jamesify.log = function(data) {
        if (Jamesify.debug) {
            console.log(data);
        }
    };

    /**
     * Jamesify.Promise
     *
     * Simple promise setup for allowing you to delay execution until
     *   something has been resolved in the promise.
     */
    Jamesify.Promise = function() {
        this.completed = false;
        this.callbacks = [];
    };

        /**
         * When will allow you to attach X number of promises to wait
         *   on the completion of, before executing your .then() callback.
         *   This is very handy for executing many ajax calls at once
         */
        Jamesify.Promise.when = function() {
            var args = [], 
                promise = new Jamesify.Promise(), 
                tasks = (1 <= arguments.length) ? Array.prototype.slice.call(arguments) : [], 
                total_not_completed = tasks.length,
                task_id,
                handle_then, 
                i;

            args.length = total_not_completed;

            handle_then = function(task_id) {
                return tasks[task_id].then(function() {
                    args[task_id] = Array.prototype.slice.call(arguments);
                    total_not_completed -= 1;
                    if (total_not_completed === 0) {
                        return promise.resolve.apply(promise, args);
                    }
                });
            };

            for (task_id = i = 0; i < tasks.length; task_id = (i+=1)) {
                handle_then(task_id);
            }

            return promise;
        };

        /**
         * Set the current promise to a resolved state and execute the callbacks
         */
        Jamesify.Promise.prototype.resolve = function() {
            var results = [],
                callback, 
                i;

            this.completed = true;
            this.data = arguments;
            
            for (i = 0; i < this.callbacks.length; i+=1) {
                callback = this.callbacks[i];
                results.push(callback.apply(callback, arguments));
            }

            return results;
        };

        /**
         * Attach a callback for execution upon the resolved state
         */
        Jamesify.Promise.prototype.then = function(callback) {
            if (this.completed === true) {
                callback.apply(callback, this.data);
                return;
            }

            return this.callbacks.push(callback);
        };
    /** END Jamesify.Promise **/

    /**
     * Jamesify.Router
     *
     * This is built to store "routes" or callbacks for a particular dom element, 
     *   and thene execute matches on the route event
     */
    Jamesify.Router = function() {
        this.routes = {};
        this.foundElement = {};
    };

        Jamesify.Router.prototype.addRoute = function(name, options, callback) {
            if (options.id === undefined && options.classes === undefined) {
                throw 'Jamesify.Router: An "id" or array of "classes" is required for routing.';
            }

            // Just assign this into the options object
            options.callback = callback;

            this.routes[name] = options;

            return this;
        };

        /**
         * Provided an event, this tries to find matches based on the
         *   loaded routes and their set of rules to match upon, 
         *   and will execute matched routes
         */
        Jamesify.Router.prototype.route = function(event) {
            var eventElement = event.target, 
                id           = event.target.id, 
                classes      = event.target.className.split(" "),
                foundRoute   = false,
                foundClasses = 0,
                name,
                route;

            for (name in this.routes) {
                // This technically doesn't pass the jslint, but i dislike nesting if's too deep
                if (!this.routes.hasOwnProperty(name)) {
                    continue;
                }

                route = this.routes[name];

                // We will let ID's be the trump card
                if (route.id !== undefined && route.id === id) {
                    this.foundElement = eventElement;
                    foundRoute = route;
                    break;
                }

                // Match provided classes
                if (this.isClassMatch(route.classes, classes)) {
                    this.foundElement = eventElement;
                    foundRoute = route;
                    break;
                }

                // If we are allowing child elements to bubble up and match, 
                // than start the recursive adventure to find a match
                if (route.allowChildren !== undefined && route.allowChildren === true) {
                    if (this.isChild(eventElement, (route.id === undefined) ? route.classes : route.id)) {
                        foundRoute = route;
                        break;
                    }
                }
            }

            // No route to send to, so lets just let the event pass-through as usual
            if (foundRoute === false) {
                Jamesify.log("No valid route was found");
                return true;
            }

            // Stop the event from going through
            event.preventDefault();
            event.stopPropagation();

            // Call the attached callback
            foundRoute.callback.call(null, this.foundElement, event);

            return false;
        };

        Jamesify.Router.prototype.isClassMatch = function(routeClasses, eventClasses) {
            var foundClasses = 0,
                i;

            if (routeClasses === undefined || !routeClasses.length) {
                return false;
            }

            // We need to find all the classes in the event,
            // that are present in our route
            for (i = 0; i < routeClasses.length; i+=1) {
                if (eventClasses.indexOf(routeClasses[i]) >= 0) {
                    foundClasses += 1;
                }
            }

            // Only if we have all of the classes provided,
            // should we consider this a match
            if (foundClasses === routeClasses.length) {
                return true;
            }

            return false;
        };

        Jamesify.Router.prototype.isChild = function(checkAgainst, deferredParentName) {
            if (checkAgainst === undefined || checkAgainst.parentElement === null) {
                return false;
            }

            if (checkAgainst.parentElement !== undefined
                && (checkAgainst.parentElement.id === deferredParentName
                    || this.isClassMatch(deferredParentName, checkAgainst.parentElement.className.split(" "))
                )
            ) {
                this.foundElement = checkAgainst.parentElement;
                return true;
            } else if (checkAgainst.parentElement.tagName !== "body") {
                return this.isChild(checkAgainst.parentElement, deferredParentName);
            }

            return false;
        };

        Jamesify.Router.registeredRouters = {};

        Jamesify.Router.addRouter = function(eventType, router) {
            document.addEventListener(eventType, Jamesify.Router.route);

            Jamesify.Router.registeredRouters[eventType] = router;

            return this;
        };

        Jamesify.Router.route = function(event) {
            return Jamesify.Router.registeredRouters[event.type].route(event);
        };
    /** END Jamesify.Router **/

    /**
     * Jamesify.View
     *
     * Simple view for replacing provided vars.  Eliminates lots of concating mainly
     */
    Jamesify.View = function(view, view_data) {
        this.view      = view      || '';
        this.view_data = view_data || {};
    };

        Jamesify.View.prototype.assign = function(view_data) {
            this.view_data = view_data;

            return this;
        };

        /**
         * Render the view.
         *
         * I wanted to just use a simple RegEx and not eval, as eval's are
         *   typically something you should avoid, and I really didn't see
         *   the need for them in this view object
         */
        Jamesify.View.prototype.render = function(view) {
            var rendered_view = view || this.view, 
                temp_rendered, 
                regex, 
                temp,
                i, 
                x;

            for (i in this.view_data) {
                if (!this.view_data.hasOwnProperty(i)) {
                    continue;
                }

                temp_rendered = this.view_data[i];
                if (typeof this.view_data[i] === "object" && this.view_data[i].length !== undefined) {
                    temp_rendered = '';
                    for (x in this.view_data[i]) {
                        if (!this.view_data[i].hasOwnProperty(x)) {
                            continue;
                        }

                        temp = this.view_data[i][x];

                        if (!(temp instanceof Jamesify.View)) {
                            break;
                        }
                        
                        temp_rendered += temp.render();                
                    }
                }

                regex = new RegExp("{{" + i + "}}", "g");
                rendered_view = rendered_view.replace(regex, temp_rendered);
            }

            return rendered_view;
        };
    /** END Jamesify.View **/

    /**
     * Jamesify.Model
     */
    Jamesify.Model = function() {
        this.observers = {};
    };

        Jamesify.Model.prototype.cacheSave = function(key, value) {
            Jamesify.log("Saving to localStorage: [" + key + "] = " + value);

            window.localStorage.setItem(key, JSON.stringify(value));

            return this;
        };

        Jamesify.Model.prototype.cacheDelete = function(key) {
            Jamesify.log("Deleting from localStorage: " + key);

            window.localStorage.removeItem(key);

            return this;
        };

        /**
         * Set the value of a field to a new value,
         *   using this had the benefit of triggering observers.
         *
         * @param {String} Field name
         * @param {String} New value
         * @return {Jamesify.Model}
         */
        Jamesify.Model.prototype.set = function(key, value) {
            var i;

            this[key] = value;

            // So we have a field change, so find it we have attached observers to call
            if (this.observers[key] !== undefined && this.observers[key].length) {
                Jamesify.log("Executing observers.");

                // Execute each observer
                for (i = 0; i < this.observers[key].length; i+=1) {
                    this.observers[key][i].call(null);
                }
            }

            return this;
        };

        Jamesify.Model.prototype.cacheGet = function(key) {
            var data = window.localStorage.getItem(key);

            Jamesify.log("Loading from localStorage: " + key);

            if (data !== null && data.length > 0) {
                data = JSON.parse(data);
            }

            return data;
        };

        /**
         * Attach a callback to a certain field to be called when a param changes
         *
         * This pattern is going to be very useful for changing views on demand
         *
         * @param {String} Field name
         * @param {Function} The callback to be attached
         * @return {Jamesify.Model}
         */
        Jamesify.Model.prototype.addObserver = function(field, callback) {
            if (this.observers[field] === undefined) {
                this.observers[field] = [];
            }
            Jamesify.log("Adding observers.");
            this.observers[field].push(callback);

            return this;
        };

        Jamesify.Model.restore = function(instance, args) {
            var i;

            for (i in args) {
                if (!args.hasOwnProperty(i)) {
                    continue;
                }

                instance[i] = args[i];
            }

            return instance;
        };
    /** END Jamesify.Model **/

    /**
     * Jamesify.Api
     */
    Jamesify.Api = function () {};

        Jamesify.Api.prototype.execute = function () {
            var promise = new Jamesify.Promise();

            // Sorry, I did the crutch move and just relied on jQuery for
            // my ajax calls
            $.getJSON(this.full_endpoint, function (data) {
                promise.resolve(data);
            });

            return promise;
        };
    /** END Jamesify.Api **/
}());